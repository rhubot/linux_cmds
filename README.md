# README #

This repo holds several batch files that, when placed in the Windows directory or elsewhere in the PATH environment variable, translates some basic Linux shell commands to Windows Command Prompt commands.

### Setting up PATH ###
If you do not wish to place the batch files in the Windows directory, you can add the folder containing the batch files to your PATH environment variable:

1. In Search, search for and then select: System (Control Panel)
1. Click the Advanced system settings link.
1. Click Environment Variables. In the section System Variables, find the PATH environment variable and select it. Click Edit. If the PATH environment variable does not exist, click New.
1. In the Edit System Variable (or New System Variable) window, specify the value of the PATH environment variable. Click OK. Close all remaining windows by clicking OK.
1. Reopen Command prompt window if any were previously open.